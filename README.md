# Sign

Helper program to generate RSA key-pair, sign input file & verify signature

## Building
------------
`go build -o sign.exe`

## Usage
---------

#### To generate RSA key pair

* Run with flag `keys-only`

* E.g:

    ```
    .\sign.exe --keys-only priv.pem pub.pem
    ```

    Generates an RSA key pair and saves pub/private key in PEM format in provided path locations

#### To sign file

* Arguments:

    * `-f test.txt`: Provide path to file for signing

    * `-o test.sig`: Output path for base64 encoded signature

    * `-k priv.pem`: File containing private key to be used for signing

* E.g.:

    ```
    .\sign.exe -f test.exe -k priv.pem -o test.sign
    ```

#### To verify file signature

* Arguments:

    * `-f test.txt`: Provide path to message file for verification

    * `-s test.sig`: Signature for provided file to be verified

    * `-k pub.pem`: File containing public key to use for verification

* E.g.:

    ```
    .\sign.exe -f test.exe -k pub.pem -s test.sign

    PASS
    ```