package main

import (
	"crypto/rand"
	"crypto/rsa"
	"flag"
	"fmt"
	"os"

	_ "crypto/sha256"
)

func main() {

	generateKeyOnlyPtr := flag.Bool("keys-only", false, "only generate cert boolean")

	verifyOnlyPtr := flag.Bool("verify-only", false, "if need to verify signs")

	filePathPtr := flag.String("f", "", "url of file to sign")
	keyPathPtr := flag.String("k", "", "private key to sign with")
	outputPathPtr := flag.String("o", "sign.base64", "path to save sign")

	signPathPtr := flag.String("s", "", "signature to be verified (when used with --verify-only)")

	flag.Parse()

	if *generateKeyOnlyPtr {
		if len(flag.Args()) != 2 {
			fmt.Println("Expected 2 file name arguments, received: ", len(flag.Args()))
			return
		}
		privKeyFilePath := flag.Arg(0)
		pubKeyFilePath := flag.Arg(1)

		fmt.Println("PUBLIC KEY OUTPUT PATH: ", pubKeyFilePath)
		fmt.Println("PRIVATE KEY OUTPUT PATH: ", privKeyFilePath)

		privKey, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			fmt.Println("Error generating key: ", err)
			return
		}

		pubKey := &privKey.PublicKey

		if err := GeneratePublicPEMFile(pubKey, pubKeyFilePath); err != nil {
			fmt.Println("Error writing public key to file: ", err)
			return
		}

		if err := GeneratePrivatePEMFile(privKey, privKeyFilePath); err != nil {
			fmt.Println("Error writing priv key to file: ", err)
			return
		}

		fmt.Println("SUCCESS")

	} else if *verifyOnlyPtr {
		if *filePathPtr == "" {
			fmt.Println("Expected file to verify path")
			return
		}

		if *signPathPtr == "" {
			fmt.Println("Expect path to signature file")
			return
		}

		if *keyPathPtr == "" {
			fmt.Println("Expected path to public key")
			return
		}

		fmt.Println("FILE TO VERIFY PATH: ", *filePathPtr)
		fmt.Println("SIGNATURE PATH: ", *signPathPtr)
		fmt.Println("PUBLIC KEY FILE PATH: ", *keyPathPtr)

		if err := verify(*filePathPtr, *signPathPtr, *keyPathPtr); err != nil {
			fmt.Println("FAIL: ", err)
			return
		}

		fmt.Println("PASS")

	} else {
		if *filePathPtr == "" {
			fmt.Println("Expected file to sign path")
			return
		}
		if *keyPathPtr == "" {
			fmt.Println("Expected path to priv key")
			return
		}
		privKey, err := loadPrivateKey(*keyPathPtr)
		if err != nil {
			fmt.Printf("Error occured reading key from %s: %v\n", *keyPathPtr, err)
			return
		}

		signBase64, err := sign(*filePathPtr, privKey)
		if err != nil {
			fmt.Printf("Error occured signing %s: %v\n", *filePathPtr, err)
			return
		}

		fileToOutput, err := os.Create(*outputPathPtr)
		if err != nil {
			fmt.Printf("Error creating output file %s: %v\n", *outputPathPtr, err)
			return
		}
		defer fileToOutput.Close()

		_, err = fileToOutput.WriteAt(signBase64, 0)
		if err != nil {
			fmt.Printf("Error writing signature to file %s: %v\n", *outputPathPtr, err)
			return
		}

	}

}
