package main

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
)

func loadPrivateKey(keyFilePath string) (*rsa.PrivateKey, error) {
	keyFile, err := os.Open(keyFilePath)
	if err != nil {
		return nil, err
	}
	defer keyFile.Close()

	b, err := ioutil.ReadAll(keyFile)
	if err != nil {
		return nil, err
	}

	derBlock, _ := pem.Decode(b)

	privKey, err := x509.ParsePKCS1PrivateKey(derBlock.Bytes)
	if err != nil {
		return nil, err
	}

	return privKey, nil
}

func loadPublicKey(keyFilePath string) (*rsa.PublicKey, error) {
	keyFile, err := os.Open(keyFilePath)
	if err != nil {
		return nil, err
	}
	defer keyFile.Close()

	b, err := ioutil.ReadAll(keyFile)
	if err != nil {
		return nil, err
	}

	derBlock, _ := pem.Decode(b)

	pubKey, err := x509.ParsePKCS1PublicKey(derBlock.Bytes)
	if err != nil {
		return nil, err
	}

	return pubKey, nil
}

func GeneratePublicPEMFile(pubKey *rsa.PublicKey, pubKeyFilePath string) error {
	f, err := os.Create(pubKeyFilePath)
	if err != nil {
		return fmt.Errorf("Error creating file %s: %v", pubKeyFilePath, err)
	}
	defer f.Close()

	der := x509.MarshalPKCS1PublicKey(pubKey)

	block := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: der,
	}

	err = pem.Encode(f, block)
	return err
}

func GeneratePrivatePEMFile(privKey *rsa.PrivateKey, privKeyFilePath string) error {
	f, err := os.Create(privKeyFilePath)
	if err != nil {
		return fmt.Errorf("Error creating file %s: %v", privKeyFilePath, err)
	}
	defer f.Close()

	der := x509.MarshalPKCS1PrivateKey(privKey)

	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: der,
	}

	err = pem.Encode(f, block)
	return err

}
