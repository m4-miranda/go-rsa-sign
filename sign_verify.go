package main

import (
	"bufio"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
)

func sign(fileToSignPath string, privkey *rsa.PrivateKey) ([]byte, error) {
	f, err := os.Open(fileToSignPath)
	if err != nil {
		return nil, fmt.Errorf("Error opening file %s: %v", fileToSignPath, err)
	}
	defer f.Close()
	reader := bufio.NewReader(f)

	hash := sha256.New()
	if _, err := reader.WriteTo(hash); err != nil {
		return nil, err
	}

	sig, err := rsa.SignPKCS1v15(rand.Reader, privkey, crypto.SHA256, hash.Sum(nil))
	if err != nil {
		return nil, err
	}

	base64EncodedLength := base64.StdEncoding.EncodedLen(len(sig))
	base64Sig := make([]byte, base64EncodedLength)

	base64.StdEncoding.Encode(base64Sig, sig)

	return base64Sig, nil
}

func verify(verifyFilePath string, sigFilePath string, pubKeyFilePath string) error {
	sigFile, err := os.Open(sigFilePath)
	if err != nil {
		return fmt.Errorf("Error opening %s: %v", sigFilePath, err)
	}
	defer sigFile.Close()

	verifyFile, err := os.Open(verifyFilePath)
	if err != nil {
		return fmt.Errorf("Error opening %s: %v", verifyFilePath, err)
	}
	defer verifyFile.Close()

	pubKey, err := loadPublicKey(pubKeyFilePath)
	if err != nil {
		return fmt.Errorf("Error loading public key from %s: %v", pubKeyFilePath, err)
	}

	base64sig, err := ioutil.ReadAll(sigFile)
	if err != nil {
		return fmt.Errorf("Error reading contents from %s: %v", sigFilePath, err)
	}

	signature, err := base64.StdEncoding.DecodeString(string(base64sig))
	if err != nil {
		return fmt.Errorf("Error decoding base64 from %s: %v", sigFilePath, err)
	}

	verifyFileHash := sha256.New()
	if _, err = bufio.NewReader(verifyFile).WriteTo(verifyFileHash); err != nil {
		return fmt.Errorf("Error writing hash for %s: %v", verifyFilePath, err)
	}

	if err := rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, verifyFileHash.Sum(nil), signature); err != nil {
		return fmt.Errorf("Error verifying signature: %v", err)
	}
	return nil
}
